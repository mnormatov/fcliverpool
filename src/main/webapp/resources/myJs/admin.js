var viewAllUserBlock = document.getElementById('viewAllUserId'),
    addNewUserBlock = document.getElementById('addNewUserId');

document.getElementById('btnViewAllUsersId').onclick = function () {

    if (viewAllUserBlock.style.display === 'none'){
        viewAllUserBlock.style.display = 'block';
        addNewUserBlock.style.display = "none";
    }
};

document.getElementById('btnAddNewUserId').onclick = function () {

    if (addNewUserBlock.style.display === 'none'){
        addNewUserBlock.style.display = 'block';
        viewAllUserBlock.style.display = "none";
    }
};

//---------------------------------------------------------------------------------------------------------
var searchId = $.trim($("#searchId").val());
var idS;
if (searchId.length <= 0){
    idS="";
}else {
    idS = document.getElementById('searchId').value;
}

var searchLogin = $.trim($("#searchLogin").val());
var loginS;
if (searchLogin.length <= 0){
    loginS="";
}else {
    loginS = document.getElementById('searchLogin').value;
}

var searchEmail = $.trim($("#searchEmail").val());
var emailS;
if (searchEmail.length <= 0){
    emailS="";
}else {
    emailS = document.getElementById('searchEmail').value;
}
//---------------------------------------------------------------------------------------------------------
$(function(){
    $('#searchId').on('keyup',function(){
        // if($('#searchId').val().length>0 || $('#searchId').val().length==0)
            $('#paginatedTable').bootstrapTable('refresh');
    })
});

$(function(){
    $('#searchLogin').on('keyup',function(){
        // if($('#searchLogin').val().length>=3 || $('#sea/rchLogin').val().length==0)
            $('#paginatedTable').bootstrapTable('refresh');
    })
});

$(function(){
    $('#searchEmail').on('keyup',function(){
        // if($('#searchEmail').val().length>=3 || $('#searchEmail').val().length==0)
            $('#paginatedTable').bootstrapTable('refresh');
    })
});

function queryParam(params) {
    return{
        order: params.order,
        limit: params.limit,
        offset: params.offset,
        searchId: $('#searchId').val(),
        searchLogin: $('#searchLogin').val(),
        searchEmail: $('#searchEmail').val()
    }
}
