var allNewsBlock = document.getElementById('allNewsBlockId'),
    allMyNewsBlock = document.getElementById('allMyNewsBlockId'),
    allAddNewsBlock = document.getElementById('allAddNewsBlockId');

document.getElementById('btnAllNewsId').onclick = function(){

    if(allNewsBlock.style.display === 'none'){
        allNewsBlock.style.display = 'block';
        allMyNewsBlock.style.display = 'none';
        allAddNewsBlock.style.display = 'none';
    }
};

document.getElementById('btnMyNewsId').onclick = function(){

    if(allMyNewsBlock.style.display === 'none'){
        allMyNewsBlock.style.display = 'block';
        allNewsBlock.style.display = 'none';
        allAddNewsBlock.style.display = 'none';
    }
};

document.getElementById('btnAddNewsId').onclick = function(){

    if(allAddNewsBlock.style.display === 'none'){
        allAddNewsBlock.style.display = 'block';
        allMyNewsBlock.style.display = 'none';
        allNewsBlock.style.display = 'none';
    }
};