<#import "common/common.ftl" as common>

<#import "/spring.ftl" as spring />
<#assign sec=JspTaglibs["http://www.springframework.org/security/tags"]/>

<@common.styles title="Admin Page">
    <link href="/resources/css/style.css" rel="stylesheet">
</@common.styles>
<#--header-->
<#include "common/header.ftl">

<div class="bgClass container-fluid" style="background-image: url(/resources/images/bg5.jpg);">
</div>

<main class="mt-5">
    <!--Main container-->

    <div class="container-fluid">
        <div class="row">

            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" style="margin-bottom: 20px;">
                            <div class="d-flex justify-content-center align-items-center">
                                <h2 class="font-weight-bold" style="margin-top: 10px; margin-bottom: 10px;">Новости</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--Card for news-->
                    <div id="tableNewOneId" style="display: block" class="col-md-6">
                        <!--Table  One-->
                        <table class="table table-borderless">
                            <tbody>
                            <#--<@spring.bind "addNewsOne"/>-->
                            <#list addNewsOne as addNewOne>
                                <tr>
                                    <th style="padding: 0;">
                                        <div class="card" style="margin-bottom: 20px;">
                                            <div class="card-body">
                                                <!--image-->
                                                <div class="row">
                                                    <div class="col-md-12" style="text-align: center">
                                                        <img id="imageNewOneId" class="img-fluid z-depth-2 rounded" src="${addNewOne.imageUrl}" style="width: 305px; height: 200px;">
                                                    </div>
                                                </div>
                                                <!--title-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h5 id="titleNewOneId" style="margin-top: 15px; height: 80px;"><a id="onclickTitleOneId" href="#">${addNewOne.title}</a> </h5>
                                                        <hr>
                                                    </div>
                                                </div>
                                                <!--content example-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p id="contentExampleNewOneId" style="margin-left: 10px; margin-right: 10px; height: 60px;">${addNewOne.contentExample}</p>
                                                    </div>
                                                </div>
                                                <!--author  and date public-->
                                                <div class="row">
                                                    <div class="col-md-12" style="text-align: center; margin-bottom: 10px;">
                                                        <hr>
                                                        <i>Автор: </i><i id="authorNewOneId">${addNewOne.author}</i> |
                                                        <i>Дата: </i><i id="dateNewOneId">${addNewOne.date}</i> |
                                                        <i id="idNewOneId" style="display: none;">${addNewOne.id}</i>
                                                        <i id="contentNewOneId" style="display: none;">${addNewOne.content}</i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </#list>

                            </tbody>
                        </table>
                        <!--Table  One-->
                    </div>
                    <div id="tableNewTwoId" style="display: block" class="col-md-6">
                        <!--Table  Two-->
                        <table class="table table-borderless" style="border-collapse: collapse; border-spacing: 0;">
                            <tbody>

                            <#list addNewsTwo as addNewTwo>
                                <tr>
                                    <th style="padding: 0;">
                                        <div class="card" style="margin-bottom: 20px;">
                                            <div class="card-body">
                                                <!--image-->
                                                <div class="row">
                                                    <div class="col-md-12" style="text-align: center">
                                                        <img class="img-fluid z-depth-2 rounded" src="${addNewTwo.imageUrl}" style="width: 305px; height: 200px;">
                                                    </div>
                                                </div>
                                                <!--title-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h5 style="margin-top: 15px; height: 80px;"><a href="#">${addNewTwo.title}</a> </h5>
                                                        <hr>
                                                    </div>
                                                </div>
                                                <!--content example-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p style="margin-left: 10px; margin-right: 10px; height: 60px;">${addNewTwo.contentExample}</p>
                                                    </div>
                                                </div>
                                                <!--author  and date public-->
                                                <div class="row" style="">
                                                    <div class="col-md-12" style="text-align: center; margin-bottom: 10px;">
                                                        <hr>
                                                        <i>Автор: ${addNewTwo.author} </i>
                                                        <i>| Дата: ${addNewTwo.date}</i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </#list>

                            </tbody>
                        </table>
                        <!--Table  Two-->
                    </div>
                    <div id="cardNewViewId" class="col-md-12" style="display: none;">
                        <div class="card">
                            <div class="d-flex justify-content-center">
                                <img id="imageCardNewViewId" class="img-fluid z-depth-2 rounded" src="/resources/imgForNews/image1.jpg"
                                style="margin: 10px;">
                            </div>
                            <h3 id="titleCardNewViewId" style="text-align: center; margin-top: 10px;"></h3>
                            <hr>
                            <p id="contentCarNewViewId" style="margin-left: 10px; margin-right: 10px;"></p>
                            <hr>
                            <div style="text-align: center; margin-bottom: 10px;">
                                <i id="authorCardNewViewId"></i> |
                                <i id="dateCardNewViewId"></i> |
                                <i id="timeCardNewViewId"></i>
                            </div>
                        </div>
                    </div>
                    <!--Card for news-->
                </div>
            </div>
            <!--Card for Turnire -->
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-center mx-auto">Турнирная таблица</h4>
                        <hr>
                        <table class="table table-striped table-sm mx-sm-auto">
                            <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>И</th>
                                <th>В</th>
                                <th>Н</th>
                                <th>П</th>
                                <th>Мячи</th>
                                <th>Очки</th>
                            </tr>
                            </thead>
                            <tbody>

                            <#list turnireTable as turnireTable>
                            <tr>
                                <#if turnireTable.id < 5>
                                    <td style="background-color: #005cbf"></td>
                                </#if>

                                <#if turnireTable.id gt 4 >
                                    <#if turnireTable.id < 8 >
                                       <td style="background-color: #bfbd01"></td>
                                    </#if>
                                </#if>

                                <#if turnireTable.id gt 7>
                                    <#if turnireTable.id < 18>
                                        <td></td>
                                    </#if>
                                </#if>

                                <#if turnireTable.id gt 17 >
                                    <td style="background-color: #bf1110"></td>
                                </#if>

                                <td>${turnireTable.id}</td>
                                <td style="text-align: left;">${turnireTable.name}</td>
                                <td>${turnireTable.games}</td>
                                <td>${turnireTable.win}</td>
                                <td>${turnireTable.nichya}</td>
                                <td>${turnireTable.porajeniye}</td>
                                <td>${turnireTable.myachi}</td>
                                <td>${turnireTable.ochku}</td>
                            </tr>
                            </#list>

                            </tbody>
                        </table>

                        <div>
                            <span style="font-size: small"><i class="fa fa-square" aria-hidden="true" style="color: #005cbf;"></i> Лига Чемпионов (групповая стадия)</span><br>
                            <span style="font-size: small"><i class="fa fa-square" aria-hidden="true" style="color: #bfbd01;"></i> Лига Европы</span><br>
                            <span style="font-size: small"><i class="fa fa-square" aria-hidden="true" style="color: #bf1110;"></i> Вылет из лиги</span><br>
                        </div>

                    </div>
                </div>
            </div>
            <!--Tabel for Turnire -->
        </div>
    </div>


    <!--Main container-->
</main>

<!--Footer-->
<#include "common/footer.ftl">
<!--/.Footer-->
<@common.script>
    <script type="text/javascript" src="/resources/myJs/homePage.js"></script>
</@common.script>