<header>
    <!--Navbar-->
    <nav id="navbarId" class="navbar navbar-expand-lg red darken-4 navbar-dark fixed-top scrolling-navbar">

        <div class="container">
            <!-- Navbar brand -->
            <a class="navbar-brand" href="#">FC Liverpool</a>

            <!-- Collapse button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Collapsible content -->
            <div class="collapse navbar-collapse" id="basicExampleNav">

                <!-- Links -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">News</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Match</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Anfield</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">History</a>
                    </li>
                    <li class="nav-item">
                        <@sec.authorize access="hasRole('ROLE_ADD_NEWS')">
                            <a class="nav-link" href="<@spring.url "/newsPage"/>">Add News</a>
                        </@sec.authorize>
                    </li>
                    <li class="nav-item">
                        <@sec.authorize access="hasRole('ROLE_ADD_NEW_USER') or hasRole('ROLE_DELETE_USER') or hasRole('ROLE_EDIT_USER')">
                            <a class="nav-link" href="<@spring.url "/admin"/>">Admin Page</a>
                        </@sec.authorize>
                    </li>

                </ul>
                <!-- Links -->
                <@sec.authorize access="!isAuthenticated()">
                    <a href="<@spring.url "/login"/>" role="button" class="btn btn-outline-amber btn-sm">Войти</a>
                </@sec.authorize>

                <@sec.authorize access="isAuthenticated()">
                    <div class="dropdown">

                        <button class="btn btn-outline-blue btn-sm dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user" aria-hidden="true"></i> Профил
                        </button>

                        <div class="dropdown-menu" style="width: 100px; height: 200px;">
                            <div class="d-flex justify-content-center align-items-center">
                                <img src="/resources/images/favicon.ico" class="rounded-circle z-depth-0" style="width: 48px; height: 48px; background-color: #9ca5ab;">
                            </div>

                            <form action="/logout" method="get">
                                <button type="submit"  class="btn btn-outline-blue btn-sm">Выйти</button>
                            </form>
                        </div>

                    </div>
                </@sec.authorize>

            </div>
            <!-- Collapsible content -->
        </div>

    </nav>
    <!--/.Navbar-->

</header>
