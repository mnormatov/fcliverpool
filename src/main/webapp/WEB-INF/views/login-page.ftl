<#import "common/common.ftl" as common>

<#import "/spring.ftl" as spring />
<#assign sec=JspTaglibs["http://www.springframework.org/security/tags"]/>

<@common.styles title="Login Page"/>

<div style="height: 100%; background-color:rgb(231, 228, 229);">
    <div class="d-flex justify-content-center align-items-center h-100">
        <div style="width: 450px; height: 450px">
            <div class="card">
                <div class="d-flex justify-content-center align-items-center" style="margin-top: 8px; margin-bottom: 5px;">
                    <img src="/resources/images/favicon.ico" style="width: 64px; height: 64px;">
                </div>
                <div class="d-flex justify-content-center align-items-center">
                    <label class="red-text">You'll Never Walk Alone</label>
                </div>
                <div class="container-fluid">

                    <form action="<@spring.url '/home'/>" method="post">
                        <div class="md-form mb-1" style="margin-left: 10px; margin-right: 10px;">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            <i class="fa fa-user prefix grey-text"></i>
                            <input type="text" name="login" id="login" class="form-control validate">
                            <label data-error="wrong" data-success="right" for="login">Your Login</label>
                        </div>
                        <div class="md-form mb-1" style="margin-left: 10px; margin-right: 10px;">
                            <i class="fa fa-lock prefix grey-text"></i>
                            <input type="password" name="password" id="password" class="form-control validate">
                            <label data-error="wrong" data-success="right" for="password">Your Password</label>
                        </div>
                        <div class="md-form mb-1 d-flex justify-content-center align-items-center">
                            <a href="<@spring.url "/registration"/>" role="button" class="btn btn-amber btn-md">Регистрация</a>
                            <button type="submit" class="btn btn-info btn-md">Войти</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

    </div>
</div>

<@common.script/>