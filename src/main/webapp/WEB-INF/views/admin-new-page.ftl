<#import "common/common.ftl" as common>

<#import "/spring.ftl" as spring />
<#assign sec=JspTaglibs["http://www.springframework.org/security/tags"]/>

<@common.styles title="Admin News Page">
    <link href="/resources/myCss/addNews.css" rel="stylesheet">
</@common.styles>

<div class="container-fluid">

    <div class="d-flex flex-wrap mb-3">
        <div class="p-2">
            <img class="rounded-circle" src="/resources/images/favicon.ico" alt="#"
            style="background-color: white; width: 64px; height: 64px; margin: 10px;">
        </div>
        <div class="align-self-center p-2">
            <button id="btnAllNewsId" class="btn btn-amber btn-sm">All News</button>
        </div>
        <div class="align-self-center p-2">
            <button id="btnMyNewsId" class="btn btn-amber btn-sm">My News</button>
        </div>
        <div class="align-self-center p-2">
            <button id="btnAddNewsId" class="btn btn-amber btn-sm">Add News</button>
        </div>
        <div class="align-self-center ml-auto p-2">
            <a href="<@spring.url "/home"/>" class="btn btn-red btn-sm">Home</a>
        </div>
    </div>

    <div style="height: 100%; background-color:rgb(231, 228, 229);">

        <!-- All News Block -->
        <div id="allNewsBlockId"    >

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-7" style="background: #1de9b6; height: 200px;">
                        <div class="card">
                            <div class="card-title" style="text-align: center; margin-top: 8px;">
                                <h4>All News</h4>
                                <hr style="margin-left: 10px; margin-right: 10px;">
                            </div>
                            <div class="card-body">
                                <table>

                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5" style="background: #005cbf; height: 200px;"></div>
                </div>
            </div>
        </div>
        <!-- All News Block -->

        <!-- All My News Block -->
        <div class="card" id="allMyNewsBlockId" style="height: 500px; margin: 10px; display: none;">My News</div>
        <!-- All My News Block -->

        <!-- Add News Block -->
        <div class="card" id="allAddNewsBlockId" style="margin: 10px; display: none;">

            <div class="row">
                <div class="col-md-9"></div>
                <div class="col-md-3">
                    <div style="margin-top: 10px; margin-left: 10px;">
                        <button id="btnPublishId" class="btn btn-indigo btn-sm"><i class="fa fa-cloud-upload"></i>Publish</button>
                        <button id="btnViewId" class="btn btn-indigo btn-sm"><i class="fa fa-eye"></i>View</button></div>
                </div>
            </div>

            <form action="/uploadFile"
                  method="post"
                  enctype="multipart/form-data">

                <div class="card-body">
                    <!--Author Date Time-->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group" style="margin-left: 12px; margin-top: 10px;">
                                <label for="authorId" style="margin-left: 5px;">Author:</label>
                                <input type="text" name="author" class="form-control" id="authorId">
                            </div>
                        </div>
                        <div class="col-md-4" style=" margin-top: 10px;">
                            <div class="form-group">
                                <label for="dateId" style="margin-left: 5px;">Date:</label>
                                <input type="date" name="date" class="form-control" id="dateId">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" style="margin-right: 12px; margin-top: 10px;">
                                <label for="timeId" style="margin-left: 5px;">Time:</label>
                                <input type="time" name="time" class="form-control" id="timeId">
                            </div>
                        </div>
                    </div>
                    <!--Author Date Time-->

                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group" style="margin-top: 15px; margin-left:15px; margin-bottom: 10px;">
                                <label for="titleId" style="margin-left: 5px;">Title:</label>
                                <textarea name="title" class="form-control rounded-3" id="titleId" rows="2"></textarea>
                            </div>
                            <div class="form-group" style="margin-top: 15px; margin-left: 15px; margin-bottom: 10px;">
                                <label for="contentExampleId" style="margin-left: 5px;">Content Example:</label>
                                <textarea name="contentExample" class="form-control rounded-3" id="contentExampleId" rows="4" placeholder="108"></textarea>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group" style="margin-right: 15px; margin-top: 15px; margin-bottom: 10px;">
                                <label for="contentId" style="margin-left: 5px;">Content:</label>
                                <textarea name="content" class="form-control rounded-3" id="contentId" rows="10"></textarea>
                            </div>
                        </div>
                    </div>

                    <!--<div class="row">
                        <div class="col-md-10">
                            <div class="form-group" style="margin-left: 14px; margin-top: 10px;">
                                <input type="file" name="file" th:value="*{file}" id="" class="form-control-file">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-light-green btn-md">Save</button>
                        </div>
                    </div>-->
                </div>
            </form>
        </div>

        <!-- Add News Block -->
    </div>
</div>

<@common.script>
    <script type="text/javascript" src="/resources/myJs/addNews.js"></script>
</@common.script>