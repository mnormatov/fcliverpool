<#import "common/common.ftl" as common>

<#import "/spring.ftl" as spring />

<@common.styles title="Admin Page">
    <link href="/resources/myCss/addNews.css" rel="stylesheet">
    <link href="/resources/css/addons/datatables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css">

</@common.styles>

<div class="container-fluid">

    <div class="d-flex flex-wrap mb-3">
        <div class="p-2">
            <img class="rounded-circle" src="/resources/images/favicon.ico" alt="#"
            style="background-color: white; width: 64px; height: 64px; margin: 10px;">
        </div>
        <div class="align-self-center p-2">
            <button id="btnViewAllUsersId" class="btn btn-amber btn-sm">View All Users</button>
        </div>
        <div class="align-self-center p-2">
            <button id="btnAddNewUserId" class="btn btn-amber btn-sm">Add New User</button>
        </div>
        <div class="align-self-center ml-auto p-2">
            <a href="<@spring.url "/home"/>" class="btn btn-red btn-sm">Home</a>
        </div>
    </div>

    <div style="height: 100%; background-color:rgb(231, 228, 229);">
        <!--View All  Users-->
        <div class="card" id="viewAllUserId" style="height: 500px; margin: 10px;">All  Users</div>
        <!--Add New User-->

        <div class="row" id="toolbar">
            <div class="form-group col-md-4">
                <div class="active-red">
                    <input class="form-control" type="search" placeholder="Id" id="searchId">
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="active-red">
                    <input class="form-control" type="search" placeholder="Login" id="searchLogin" >
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="active-red">
                    <input class="form-control" type="search" placeholder="Email" id="searchEmail">
                </div>
            </div>
        </div>


        <div class="card" id="addNewUserId" style="margin: 10px; display: none; padding: 30px;">
                <table
                        id="paginatedTable"
                        data-toolbar="#toolbar"
                        data-toggle="table"
                        data-url="/admin/pagination"
                        data-pagination="true"
                        data-side-pagination="server"
                        data-page-list="5, 10, 20 , 50, 100, 200"
                        data-show-refresh="true"
                        data-query-params="queryParam">
                        <#--data-sort-name="id"-->
                        <#--data-sort-order="asc">-->
                    <thead>
                        <tr>
                            <th data-sortable="true" data-field="id">Id</th>
                            <th data-field="login">Login</th>
                            <th data-field="email">Email</th>
                            <th data-field="firstName">First Name</th>
                            <th data-field="lastName">Last Name</th>
                            <th data-field="roleNames">User Role</th>
                        </tr>
                    </thead>

                </table>
        </div>
    </div>

</div>

<@common.script>
        <script type="text/javascript" src="/resources/js/addonsJs/datatables.min.js"></script>

    <script href="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript"></script>
    <script href="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.js" type="text/javascript"></script>

    <!-- Latest compiled and minified Locales -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-ru-RU.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="/resources/myJs/admin.js" defer></script>


<script>


</script>

</@common.script>

