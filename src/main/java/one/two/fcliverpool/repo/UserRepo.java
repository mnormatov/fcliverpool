package one.two.fcliverpool.repo;

import one.two.fcliverpool.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepo extends JpaRepository<User, Integer> {

    @Query("select u from User u where u.login= :login")
    User findByLogin(@Param("login") String login);

    @Query("select u from User u order by u.id")
    Page<User> findAllUsers(@Param("id") int id, Pageable pageable);

    @Query("select count(id) from User ")
    int getRowCount();

    @Query("select u from User u where u.login like concat(:#{#login},'%')")
    Page<User> findUserByParams(@Param("login") String login, Pageable pageable);

}