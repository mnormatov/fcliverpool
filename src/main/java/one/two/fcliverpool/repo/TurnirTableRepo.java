package one.two.fcliverpool.repo;

import one.two.fcliverpool.model.TurnireTable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TurnirTableRepo extends JpaRepository<TurnireTable, Integer> {
}
