package one.two.fcliverpool.repo;

import one.two.fcliverpool.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RoleRepo extends JpaRepository<Role, Integer> {

    @Query("select r.roleName from User u ,UserRole ur, Role r where u.id = :userId and u.id=ur.userId and ur.roleId=r.id")
    List<String> getRoleName(@Param("userId") Integer userId);

    @Query("select r from Role r")
    List<Role> findAll();
}
