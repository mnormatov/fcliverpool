package one.two.fcliverpool.repo;

import one.two.fcliverpool.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleUserRepo extends JpaRepository<UserRole, Integer> {
}
