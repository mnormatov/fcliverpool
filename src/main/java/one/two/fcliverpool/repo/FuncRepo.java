package one.two.fcliverpool.repo;

import one.two.fcliverpool.model.Func;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FuncRepo extends JpaRepository<Func, String> {

    @Query("select f from Role r, FuncRole fr, Func f where r.id= :id and r.id=fr.roleId and fr.funcId=f.id")
    //@Query("select f from User u, UserRole ur, Role r, FuncRole fr, Func f where u.id= :id and u.id=ur.userId and ur.roleId=fr.roleId and fr.funcId=f.id")
    List<Func> findRoleById(@Param("id") Integer id);

}
