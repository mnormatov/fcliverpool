package one.two.fcliverpool.repo;

import one.two.fcliverpool.model.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface NewsRepo extends JpaRepository<News, Integer> {

//    @Query("select n from News n where n.table = :tableName")
//    List<News> getNews(@Param("table") String tableName);

    @Query(value = "select n from News n where n.table = ?1")
    List<News> findAllByTable(String tableName);

}
