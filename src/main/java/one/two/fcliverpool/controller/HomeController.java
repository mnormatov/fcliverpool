package one.two.fcliverpool.controller;

import one.two.fcliverpool.model.News;
import one.two.fcliverpool.model.TurnireTable;
import one.two.fcliverpool.model.User;
import one.two.fcliverpool.service.newsService.NewsService;
import one.two.fcliverpool.service.turnirTableService.TurnirTableService;
import one.two.fcliverpool.service.userService.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    TurnirTableService turnirTableService;

    @Autowired
    NewsService newsService;

    @Autowired
    UserService userService;

    @GetMapping(value = {"/", "/home"})
    public String homePage(Model model){


        List<TurnireTable> turnirTableList = turnirTableService.getTurnireTable();
        model.addAttribute("turnireTable", turnirTableList);

        List<News> newsListOne =  newsService.getNews("tableOne");
        model.addAttribute("addNewsOne", newsListOne);

        List<News> newsListTwo =  newsService.getNews("tableTwo");
        model.addAttribute("addNewsTwo", newsListTwo);

        model.addAttribute("userRegistration", new User());

        return "home-page";
    }

   @GetMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response){

       Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
       if (authentication != null) {
           new SecurityContextLogoutHandler().logout(request, response, authentication);
       }

       return "redirect:/home";
   }


}
