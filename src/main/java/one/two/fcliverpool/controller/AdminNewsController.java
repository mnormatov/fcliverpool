package one.two.fcliverpool.controller;

import one.two.fcliverpool.service.newsService.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdminNewsController {

    private static String UPLOAD_LOCATION = "D:/JavaWeb/FCLiverpool/src/main/webapp/resources/imgForNews/";

    @Autowired
    NewsService newsService;

    @GetMapping("/newsPage")
    public String getNewsPage(Model model){

        return "admin-new-page";
    }

//    @PostMapping("/uploadFile")
//    public String uploadFile(@Valid @ModelAttribute("addTempNew") TempNews tempNews,
//                            BindingResult bindingResult,
//                            Model model) throws IOException {
//
//        if (bindingResult.hasErrors()) {
//            return "admin-new-page";
//        }else {
//
//            //tempNews.setImageUrl("/resources/imgForNews/" + fileName);
//            newsService.addTempNews(tempNews);
//            return "redirect:/newsPage";
//        }
//    }
}
