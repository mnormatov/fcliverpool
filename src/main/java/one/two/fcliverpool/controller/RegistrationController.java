package one.two.fcliverpool.controller;

import one.two.fcliverpool.model.User;
import one.two.fcliverpool.service.userService.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    @Autowired
    UserService userService;

    @GetMapping("/registration")
    public String getRegistration(Model model){

        model.addAttribute("userRegistration", new User());

        return "registration-page";
    }

    @PostMapping("/registration")
    public String registrationPage(@Valid @ModelAttribute("userRegistration")
                                           User user, BindingResult bindingResult){

        if (bindingResult.hasErrors()){
            return "registration-page";
        }else {
            userService.registrationUser(user);
            return "redirect:/login";
       }
    }
}
