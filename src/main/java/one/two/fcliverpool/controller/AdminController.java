package one.two.fcliverpool.controller;

import one.two.fcliverpool.dto.UserJson;
import one.two.fcliverpool.repo.UserRepo;
import one.two.fcliverpool.service.userService.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AdminController {

    @Autowired
    UserRepo userRepo;

    @Autowired
    UserService userService;

    @GetMapping("/admin")
    public String getAdminPage(){

        return "admin-page";
    }


    @GetMapping("/admin/pagination")
    @ResponseBody
    public UserJson getAllUsers (
            @RequestParam(value = "order") String orderName,
            @RequestParam(value = "offset", defaultValue = "0") int offset,
            @RequestParam(value = "limit") int limit,
            @RequestParam(value = "searchId") Integer searchId,
            @RequestParam(value = "searchLogin") String searchLogin,
            @RequestParam(value = "searchEmail") String searchEmail){

        String order;
        if (orderName.equals("asc")){
            order = "asc";
        }else order = "desc";

        return new UserJson(userRepo.getRowCount(), userService.getAllUserList(offset, limit, order, searchId, searchLogin, searchEmail));
    }

}
//    //            @RequestParam(value = "searchId") int searchId,
//    //@RequestParam(value = "searchEmail") String searchEmail
//    @RequestParam(value = "searchLogin") String searchLogin