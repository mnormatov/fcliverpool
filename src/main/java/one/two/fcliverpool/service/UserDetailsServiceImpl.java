package one.two.fcliverpool.service;

import one.two.fcliverpool.model.Func;
import one.two.fcliverpool.model.User;
import one.two.fcliverpool.repo.FuncRepo;
import one.two.fcliverpool.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepo userRepo;

    @Autowired
    FuncRepo funcRepo;

    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        User user = userRepo.findByLogin(login);
        if(user != null) {
            List<Func> functions = funcRepo.findRoleById(user.getId());

            List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            for(Func func:functions)
                authorities.add(new SimpleGrantedAuthority(func.getId()));

            return buildUserForAuthentication(user, authorities);
        }
        throw new UsernameNotFoundException("Will be Error");
    }

    private org.springframework.security.core.userdetails.User buildUserForAuthentication(User user,
                                                                                          List<GrantedAuthority> authorities) {

        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), authorities);
    }

}
