package one.two.fcliverpool.service.turnirTableService;

import one.two.fcliverpool.model.TurnireTable;

import java.util.List;

public interface TurnirTableService {

    List<TurnireTable> getTurnireTable();

}
