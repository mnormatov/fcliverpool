package one.two.fcliverpool.service.turnirTableService;

import one.two.fcliverpool.model.TurnireTable;
import one.two.fcliverpool.repo.TurnirTableRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class TurnirTableServiceImpl implements TurnirTableService {

    @Autowired
    TurnirTableRepo turnirTableRepo;

    @Override
    @Transactional
    public List<TurnireTable> getTurnireTable() {
        return turnirTableRepo.findAll();
    }
}
