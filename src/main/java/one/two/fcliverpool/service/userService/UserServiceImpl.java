package one.two.fcliverpool.service.userService;

import one.two.fcliverpool.dto.UserJson;
import one.two.fcliverpool.dto.UserJsonList;
import one.two.fcliverpool.model.User;
import one.two.fcliverpool.model.UserRole;
import one.two.fcliverpool.repo.RoleRepo;
import one.two.fcliverpool.repo.RoleUserRepo;
import one.two.fcliverpool.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private String roleName;

    @Autowired
    EntityManager entityManager;

    @Autowired
    UserRepo userRepo;

    @Autowired
    RoleRepo roleRepo;

    @Autowired
    RoleUserRepo roleUserRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void registrationUser(User user) {

        User newUser = new User(
                user.getLogin(),
                user.getEmail(),
                passwordEncoder.encode(user.getPassword()));

        userRepo.save(newUser);

        UserRole roleUser = new UserRole(3, newUser.getId());

        roleUserRepo.save(roleUser);
    }

    @Override
    @Transactional
    public List<User> getAllUsers() {

        return userRepo.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserJsonList> getAllUserList(int offset, int limit, String order, Integer searchId, String searchLogin, String searchEmail) {

        Query query = entityManager.createNativeQuery("select u.id, u.login, u.email, un.first_name, un.last_name, group_concat(r.role_name) " +
                "from user u " +
                "join user_role ur on u.id = ur.user_id " +
                "join role r on ur.role_id = r.id " +
                "join user_user_name uun on u.id = uun.user_id " +
                "join user_name un on uun.user_name_id = un.id " +
                "where if(:searchId is not null, lower(u.id) like lower(concat('%', :searchId, '%')), u.id) " +
                "and if(:searchLogin is not null, lower(u.login) like lower(concat('%', :searchLogin, '%')), u.login) " +
                "and if(:searchEmail is not null, lower(u.email) like lower(concat('%', :searchEmail, '%')), u.email) " +
                "group by u.id " +
                "order by case when :sortName='asc' then u.id end ASC, case when :sortName='desc' then u.id end DESC " +
                "limit :lim offset :off")
                .setParameter("sortName", order)
                .setParameter("lim", limit)
                .setParameter("off", offset)
                .setParameter("searchId", searchId)
                .setParameter("searchLogin", searchLogin)
                .setParameter("searchEmail", searchEmail);

        List<UserJsonList> userJsonLists = new ArrayList<>();

        List list = query.getResultList();
        for (Object aList : list) {
            Object row[] = (Object[]) aList;
            userJsonLists.add(new UserJsonList(
                    row[0],
                    row[1],
                    row[2],
                    row[3],
                    row[4],
                    row[5]
            ));
        }

        return userJsonLists;
    }

}
