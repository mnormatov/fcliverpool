package one.two.fcliverpool.service.userService;

import one.two.fcliverpool.dto.UserJsonList;
import one.two.fcliverpool.model.User;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface UserService {

    void registrationUser(User user);

    List<User> getAllUsers();

    List<UserJsonList> getAllUserList(int offset, int limit, String order, Integer searchId, String searchLogin, String searchEmail);

}
