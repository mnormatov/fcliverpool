package one.two.fcliverpool.service.newsService;

import one.two.fcliverpool.model.News;
import one.two.fcliverpool.repo.NewsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {

    @Autowired
    NewsRepo newsRepo;

    @Override
    @Transactional
    public List<News> getNews(String tableName) {

        return newsRepo.findAllByTable(tableName);
    }
}
