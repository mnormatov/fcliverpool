package one.two.fcliverpool.service.newsService;

import one.two.fcliverpool.model.News;

import java.util.List;

public interface NewsService {

    List<News> getNews(String tableName);

}
