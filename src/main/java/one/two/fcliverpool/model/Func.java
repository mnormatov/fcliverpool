package one.two.fcliverpool.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Func {
    private String id;
    private String funcName;

    @Id
    @Column(name = "id", nullable = false, length = 255)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "func_name", nullable = false, length = 255)
    public String getFuncName() {
        return funcName;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Func func = (Func) o;
        return Objects.equals(id, func.id) &&
                Objects.equals(funcName, func.funcName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, funcName);
    }
}
