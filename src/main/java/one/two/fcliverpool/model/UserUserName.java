package one.two.fcliverpool.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_user_name", schema = "liverpool_site", catalog = "")
public class UserUserName {
    private Integer id;
    private Integer userId;
    private Integer userNameId;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_id")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "user_name_id")
    public Integer getUserNameId() {
        return userNameId;
    }

    public void setUserNameId(Integer userNameId) {
        this.userNameId = userNameId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserUserName that = (UserUserName) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(userNameId, that.userNameId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, userId, userNameId);
    }
}
