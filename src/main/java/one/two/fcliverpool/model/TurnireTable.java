package one.two.fcliverpool.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "turnire_table", schema = "liverpool_site", catalog = "")
public class TurnireTable {
    private Integer id;
    private String name;
    private String games;
    private String win;
    private String nichya;
    private String porajeniye;
    private String myachi;
    private String ochku;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "games", nullable = false, length = 255)
    public String getGames() {
        return games;
    }

    public void setGames(String games) {
        this.games = games;
    }

    @Basic
    @Column(name = "win", nullable = false, length = 255)
    public String getWin() {
        return win;
    }

    public void setWin(String win) {
        this.win = win;
    }

    @Basic
    @Column(name = "nichya", nullable = false, length = 255)
    public String getNichya() {
        return nichya;
    }

    public void setNichya(String nichya) {
        this.nichya = nichya;
    }

    @Basic
    @Column(name = "porajeniye", nullable = false, length = 255)
    public String getPorajeniye() {
        return porajeniye;
    }

    public void setPorajeniye(String porajeniye) {
        this.porajeniye = porajeniye;
    }

    @Basic
    @Column(name = "myachi", nullable = false, length = 255)
    public String getMyachi() {
        return myachi;
    }

    public void setMyachi(String myachi) {
        this.myachi = myachi;
    }

    @Basic
    @Column(name = "ochku", nullable = false, length = 255)
    public String getOchku() {
        return ochku;
    }

    public void setOchku(String ochku) {
        this.ochku = ochku;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TurnireTable that = (TurnireTable) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(games, that.games) &&
                Objects.equals(win, that.win) &&
                Objects.equals(nichya, that.nichya) &&
                Objects.equals(porajeniye, that.porajeniye) &&
                Objects.equals(myachi, that.myachi) &&
                Objects.equals(ochku, that.ochku);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, games, win, nichya, porajeniye, myachi, ochku);
    }
}
