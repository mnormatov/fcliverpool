package one.two.fcliverpool.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "func_role", schema = "liverpool_site", catalog = "")
public class FuncRole {
    private Integer id;
    private String funcId;
    private Integer roleId;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "func_id", nullable = false, length = 255)
    public String getFuncId() {
        return funcId;
    }

    public void setFuncId(String funcId) {
        this.funcId = funcId;
    }

    @Basic
    @Column(name = "role_id", nullable = false)
    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FuncRole funcRole = (FuncRole) o;
        return Objects.equals(id, funcRole.id) &&
                Objects.equals(funcId, funcRole.funcId) &&
                Objects.equals(roleId, funcRole.roleId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, funcId, roleId);
    }
}
