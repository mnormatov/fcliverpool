package one.two.fcliverpool.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class News {
    private Integer id;
    private String table;
    private String title;
    private String content;
    private String contentExample;
    private String imageUrl;
    private String author;
    private Timestamp date;
    private String time;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "table", nullable = false, length = 255)
    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    @Basic
    @Column(name = "title", nullable = false, length = 255)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "content", nullable = false, length = -1)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "content_example", nullable = false, length = -1)
    public String getContentExample() {
        return contentExample;
    }

    public void setContentExample(String contentExample) {
        this.contentExample = contentExample;
    }

    @Basic
    @Column(name = "image_url", nullable = false, length = 255)
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Basic
    @Column(name = "author", nullable = false, length = 255)
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Basic
    @Column(name = "date", nullable = false)
    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @Basic
    @Column(name = "time", nullable = false, length = 255)
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        News news = (News) o;
        return Objects.equals(id, news.id) &&
                Objects.equals(table, news.table) &&
                Objects.equals(title, news.title) &&
                Objects.equals(content, news.content) &&
                Objects.equals(contentExample, news.contentExample) &&
                Objects.equals(imageUrl, news.imageUrl) &&
                Objects.equals(author, news.author) &&
                Objects.equals(date, news.date) &&
                Objects.equals(time, news.time);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, table, title, content, contentExample, imageUrl, author, date, time);
    }
}
