package one.two.fcliverpool.dto;

public class UserJsonList {

    private Integer id;
    private String login;
    private String email;
    private String firstName;
    private String lastName;
    private String roleNames;

    public UserJsonList(Object id, Object login, Object email, Object firstName, Object lastName, Object roleNames) {
        this.id = (Integer) id;
        this.login = (String) login;
        this.email = (String) email;
        this.firstName = (String) firstName;
        this.lastName = (String) lastName;
        this.roleNames = (String) roleNames;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRoleNames() {
        return roleNames;
    }

    public void setRoleNames(String roleNames) {
        this.roleNames = roleNames;
    }
}
