package one.two.fcliverpool.dto;

import java.util.List;

public class UserJson {

    private int total;
    private List<UserJsonList> rows;

    public UserJson() {
    }

    public UserJson(int total, List<UserJsonList> rows) {
        this.total = total;
        this.rows = rows;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<UserJsonList> getRows() {
        return rows;
    }

    public void setRows(List<UserJsonList> rows) {
        this.rows = rows;
    }
}
